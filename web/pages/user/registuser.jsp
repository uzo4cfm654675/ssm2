<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票系统注册页面</title>
<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">
		$(function () {
			<c:if test="${not empty requestScope.alert}">
			alert("${requestScope.alert}")
			</c:if>

			$("#code_img").click(function () {
				this.src="../Kaptcha?d="+new Date();
			});
			$("#sub_btn").click(function () {

				var  password=$("#password").val();

				//确认密码
				var passwordtest=$("#pass").val();

				if (passwordtest!=password){
					$("span.errorMsg").text("密码确认失败");
					return false;
				}
				// 验证码
				var code=$("#code").val();
				var code=$.trim(code);
				if (code==null|| code==""){
					$("span.errorMsg").text("验证码为空");
					return false;
				}
				$("span.errorMsg").text("");
			});

		});

	</script>
<style type="text/css">
	.login_form{
		height:600px;
		margin-top: 25px;
	}
	
</style>


</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>用户注册</h1>
								<span class="errorMsg"></span>
							</div>
							<div class="form">

								<form:form modelAttribute="user" action="/registuser" method="post" >

									<form:hidden path="id"></form:hidden>
									用户名:<form:input path="name"></form:input>
									<form:errors path="name"></form:errors>
									<br>
									密码：<input type="password" id="pass">
									<br>
									确认密码:<form:password path="password" title="password" ></form:password>
									<form:errors path="password"></form:errors>
									<br>
									邮箱:<form:input path="email"></form:input>
									<form:errors path="email"></form:errors>
									<br>
									验证码：<input type="text" name="code" id="code">  <img src="../Kaptcha" id="code_img">
									<br>
									<input id="sub_btn" type="submit" value="Submit">


								</form:form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<%@include file="/pages/common/foote.jsp"%>
</body>
</html>