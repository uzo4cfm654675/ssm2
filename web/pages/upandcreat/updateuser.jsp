<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>修改个人信息</title>
    <%@include file="/pages/common/head.jsp"%>

    <script type="text/javascript">
        $(function () {
            <c:if test="${not empty requestScope.alert}">
            alert("${requestScope.alert}")

            </c:if>


            $("#code_img").click(function () {
                this.src="../Kaptcha?d="+new Date();
            });
            $("#update").click(function () {

                if( !confirm('你确定要提交修改信息吗？')){
                    return false;
                }
            })
        });

    </script>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-top: 200px;
        }

        h1 a {
            color:red;
        }

        input {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="../../static/img/logo.gif" >
    <span class="wel_word">修改用户密码</span>
    <%@include file="/pages/common/manager_menu.jsp"%>
</div>

<div id="main">


    <form:form action="/upuser" method="post" modelAttribute="user">
        用户名：<form:input path="name"></form:input>
        <br>
        密码：<form:input path="password"></form:input>
        <br>
        验证码：<input type="text" name="code">  <img src="../Kaptcha" id="code_img">
        <br>
        <input  id="update" type="submit" value="修改">

    </form:form>







</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>