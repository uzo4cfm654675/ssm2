<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>查看评委界面</title>
	<%@include file="/pages/common/head.jsp"%>

<%--	<script type="text/javascript">--%>
<%--		$(function () {--%>

<%--			$("a.vote").click(function () {--%>
<%--				confirm("你确定给"+$(this).parent().parent().find("td:first").text()+"投票吗？")--%>
<%--			});--%>
<%--		});--%>

<%--	</script>--%>



</head>
<body>
<%--<%--%>
<%--    //页面每隔30秒自动刷新一遍--%>
<%--    response.setHeader("refresh","5");--%>
<%--%>--%>

<%--<c:if test="${empty requestScope.users}">--%>

<%--	<%--%>
<%--		request.getRequestDispatcher("userServlet?action=getUsersToindex").forward(request,response);--%>
<%--	%>--%>

<%--</c:if>--%>



<div id="header">
	<img class="logo_img" alt="" src="../../static/img/logo.gif" >
	<span class="wel_word">投你票了的评委查看处</span>
	<div>

		<c:if test="${ empty sessionScope.user}">
			<a href="pages/user/login.jsp">登录</a>
			<a href="pages/user/registuser.jsp">注册</a>
		</c:if>
		<c:if test="${not empty sessionScope.user}">
			<span class="wel_word">你好${sessionScope.user.username}</span>
			<a href="userServlet?action=logout">注销</a>&nbsp;
			<a href="pages/user/login_success.jsp">返回项目管理</a>
			<a href="userServlet?action=getUsers">去投票</a>
		</c:if>

	</div>
</div>
<div id="main">
	<table>
		<tr>
			<td>评委用户名</td>
			<td></td>
<%--			<td>项目展示</td>--%>
<%--			<td></td>--%>
<%--			<td>总票数</td>--%>
<%--			<td></td>--%>


		</tr>

		<c:if test="${not empty requestScope.jdusers}">
			<c:forEach items="${requestScope.jdusers}" var="jduser">



					<tr>
						<td>${jduser.jdname}</td>
						<td></td>


					</tr>


			</c:forEach>
		</c:if>

		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<%--				<td><a href="pages/manager/book_edit.jsp">添加图书</a></td>--%>
		</tr>
	</table>
</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>