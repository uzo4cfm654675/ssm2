package zyc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zyc.bean.Project;
import zyc.dao.ProjectMapper;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 10:59
 * @version: v1.0
 * @modified By:
 */
@Service
public class ProjectService {

    @Autowired
    private ProjectMapper projectMapper;

    public List<Project> getProjects(Project project) {
        return projectMapper.getProjects(project);

    }

    public Project getproject(Project project) {
        return projectMapper.getproject(project);
    }

    public void deletProjectByid(Integer id) {
        projectMapper.deletProjectByid(id);
    }

    public void creatProject(Project project) {
        projectMapper.insertProject(project);
    }

    public void setPoll(Project project) {
        projectMapper.updateProject(project);
    }

    public Project getprojectById(Integer id) {
        return projectMapper.getprojectById(id);
    }

    public void updateConten(Project project) {
        projectMapper.updateProject(project);
    }
}
