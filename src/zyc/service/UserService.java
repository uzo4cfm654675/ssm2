package zyc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zyc.bean.User;
import zyc.dao.UserMapper;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 10:59
 * @version: v1.0
 * @modified By:
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public List<User> getUsers() {
        return userMapper.selectUsers();


    }

    public void registuser(User user) {
        userMapper.inseruser(user);
    }

    public User selectUser(User user) {
        return userMapper.selectUser(user);
    }

    public void update(User user) {
        userMapper.updateuser(user);
    }
}
