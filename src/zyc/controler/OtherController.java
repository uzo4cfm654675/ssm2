package zyc.controler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import zyc.bean.User;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/9 23:38
 * @version: v1.0
 * @modified By:
 */
@Controller
public class OtherController {

//    @RequestMapping("/sendEmail")
//    public String sendEmail(@RequestParam("user")User user){
//
//    }
    //随机验证码函数
    public String getcode() {
        int a = (int) (Math.random() * (9999 - 1000) + 1000);
        String c = a + "";
        return c;
    }

    //发送邮件的方法
    public void email(String code, String email) throws Exception {
        Properties properties = new Properties();

        properties.put("mail.transport.protocol", "smtp");//连接协议

        properties.put("mail.smtp.host", "smtp.qq.com");//主机名

        properties.put("mail.smtp.port", 465);//端口号

        properties.put("mail.smtp.auth", "true");

        properties.put("mail.smtp.ssl.enable", "true");//设置是否使用ssl安全连接--使用

        properties.put("mail.debug", "true");//设置是否显示debug信息true会在控制台显示相关信息
        //得到回话对象
        Session session = Session.getInstance(properties);
        //获取邮件对象
        Message message = new MimeMessage(session);
        //设置发件人邮箱地址
        message.setFrom(new InternetAddress("1049566924@qq.com"));
        //设置收件人邮箱地址
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        //设置邮件标题
        message.setSubject("验证码到了");
        //设置邮件内容
        message.setText("您的验证码是：" + code + "(请在五分钟内完成验证否则失效)");
        //得到邮差对象
        Transport transport = session.getTransport();
        //连接自己的邮箱账户
        transport.connect("1049566924@qq.com", "zxuvxjfybjoabdfi");//password为QQ邮箱开通smtp服务后得到的授权码

        //发送邮件
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
}
