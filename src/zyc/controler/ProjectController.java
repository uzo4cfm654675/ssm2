package zyc.controler;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import zyc.bean.Project;
import zyc.bean.User;
import zyc.service.ProjectService;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/29 11:16
 * @version: v1.0
 * @modified By:
 */
@Controller

public class ProjectController {

    @Autowired
    private ProjectService projectService;

    //修改项目的poll
    @RequestMapping("/setPoll")
    public String setPoll(@RequestParam("id") Integer id, @RequestParam("poll") Integer poll) {


        projectService.setPoll(new Project(id, null, poll, new User()));
        return "show/showprojecet";

    }


    // 创建和修改过审的项目  前获取信息
    @RequestMapping(value = "/project", method = RequestMethod.GET)
    public String inserProject(@RequestParam(value = "id") Integer id, Map<String, Object> map) {

        Project project;
        if(id==0){
            project=new Project();
        }else{
            project=projectService.getproject(new Project(id,null,null,new User()));
        }



        map.put("project",project);


        return "upandcreat/creat_pr";

    }



    //删除项目
    @RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE)
    public String deletProject(@PathVariable("id") Integer id) {
        Project project = projectService.getprojectById(id);
        projectService.deletProjectByid(id);
        return "forward:/setTarget?uid=" + project.getUser().getId()+"&pid="+0;
    }


    //用户通过 用户id 查询到  projects  并跳转到  登录和成功页面  所有到 登录成功的页面都必须经过这里


    @RequestMapping("/getprojects/{u_id}")
    public String getprojectByuid(@PathVariable(value = "u_id") Integer id, Map<String, Object> map) {


        Project project = new Project(null, null, null, new User(id));


        List<Project> projects = projectService.getProjects(project);
        map.put("projects", projects);
        return "user/login_success";

    }

    //观看所有的  项目
    @RequestMapping("/getProjects")
    public String getProjects(Map<String, Object> map) {
        List<Project> projects = projectService.getProjects(null);
        map.put("projects", projects);
        return "forward:/index.jsp";
    }

    //获取所有项目 去投票

    @RequestMapping("/getProjectsToTou")
    public String getProjectsToTou(Map<String, Object> map) {
        List<Project> projects = projectService.getProjects(null);
        map.put("projects", projects);
        map.put("head", "请选择：(一人只能投一个)");
        return "show/showprojecet";
    }



    //获取单个项目 告诉用户 已经有投票对象
    @RequestMapping(value = "/getProjectToTou/{target}", method = RequestMethod.GET)
    public String getProjectToTou(@PathVariable("target") Integer id, Map<String, Object> map) {


        Project project = projectService.getprojectById(id);

        map.put("project", project);
        map.put("head", "您的投票对象如下：(一人只能投一个)");
        return "show/showprojecet";
    }


}
