package zyc.dao;


import org.springframework.stereotype.Repository;
import zyc.bean.Project;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/11/20 17:56
 * @version: v1.0
 * @modified By:
 */
@Repository
public interface T_projectMapper {


    List<Project > getProjects(Project  project);

    Project  getproject(Project  t_project);

    void creatProject(Project project);

    void updateConten(Project project);
}
