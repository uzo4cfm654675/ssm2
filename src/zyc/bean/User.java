package zyc.bean;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/12/1 22:39
 * @version: v1.0
 * @modified By:
 */
public class User {
    private Integer id;
    @NotEmpty(message = "名字不能为空")
    private String name;
    @NotEmpty(message = "密码不能为空")
    private  String password;
    @Email(message = "邮箱格式有问题")
    private String email;
    private Integer target=0;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", target=" + target +
                '}';
    }

    public User(Integer id, String name, String password, String email, Integer target) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
        this.target = target;
    }

    public Integer getTarget() {

        return target;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public User(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(Integer id, String name, String password, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public User() {
    }
}
